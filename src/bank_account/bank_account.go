package account

import "sync"

type Account struct{
		sync.Mutex
		deposit int64
		closed bool
	}

func Open(initialDeposit int64) *Account{
	if initialDeposit<0 {return nil}
	return &Account{deposit: initialDeposit,closed: false}
}

func (a *Account) Balance() (balance int64, ok bool){
	a.Lock()
	defer a.Unlock()
	return a.deposit, !a.closed
}

func (a *Account) Deposit(amount int64) (newBalance int64, ok bool){
	a.Lock()
	defer a.Unlock()
	if a.closed || a.deposit+amount<0 {return 0,false}
	a.deposit += amount
	return a.deposit, true
}

func (a *Account) Close() (payout int64, ok bool){
	a.Lock()
	defer a.Unlock()
	if a.closed {return 0,false}
	payout = a.deposit
	a.deposit=0
	a.closed=true
	return payout, true
}