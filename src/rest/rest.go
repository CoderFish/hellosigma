package rest

import (
	"net/http"
	"log"
	"encoding/json"
	"strconv"
	"net/url"
	"bank_account"
)

var acc *account.Account

func setContent(res http.ResponseWriter,statusCode int, key,val string){
	e:=url.Values{}
	res.Header().Set("Content-type", "applciation/json")
	res.WriteHeader(statusCode)
	e.Add(key,val)
	json.NewEncoder(res).Encode(e)
}

func accountAPI(res http.ResponseWriter, req *http.Request) {
	req.ParseForm()

	if req.Method=="GET" {
		if acc==nil{
			setContent(res,http.StatusBadRequest,"Error","Account does not exist")
			return
		}
		depo,ok := acc.Balance()
		if !ok {
			setContent(res,http.StatusBadRequest,"Error","Account is closed")
			return
		}
		setContent(res,http.StatusOK,"amount",strconv.Itoa(int(depo)))
		return
	}

	if req.Method=="POST" {
		if acc==nil{
			setContent(res,http.StatusBadRequest,"Error","Account does not exist")
			return
		}
		val, ok := req.Form["amount"]
		if !ok {
			setContent(res,http.StatusBadRequest,"Error","Param name does not exist")
			return
		}
		amount, err := strconv.ParseInt(val[0], 10, 64)
		if err!=nil {
			setContent(res,http.StatusBadRequest,"Error","Wrong param value, account is not created")
			return
		}
		_,ok = acc.Balance()
		if !ok {
			setContent(res,http.StatusBadRequest,"Error","Sorry account already closed")
			return
		}
		_,ok = acc.Deposit(amount)
		if !ok {
			setContent(res,http.StatusBadRequest,"Error","Not enough money")
			return
		}
		return
	}

	if req.Method=="PUT" {
		if acc!=nil{//if account exist and closed, we can destroy old account, and allow to recreate it
			if _,ok := acc.Balance(); !ok{
				acc=nil
			}else{
				setContent(res,http.StatusBadRequest,"Error","Account already exist and not closed, account is not created")
				return
			}
		}
		val, ok := req.Form["initialAmount"]
		if !ok {
			setContent(res,http.StatusBadRequest,"Error","Param name does not exist, account is not created")
			return
		}
		amount, err := strconv.ParseInt(val[0], 10, 64)
		if err!=nil {
			setContent(res,http.StatusBadRequest,"Error","Wrong param value, account is not created")
			return
		}
		acc = account.Open(amount)
		if acc==nil{
			setContent(res,http.StatusBadRequest,"Error","Account is not created")
			return
		}
		return
	}

	if req.Method=="DELETE" {
		if acc==nil{
			setContent(res,http.StatusBadRequest,"Error","Can't delete account, it was not created")
			return
		}
		payout,ok := acc.Close()
		if !ok {
			setContent(res,http.StatusBadRequest,"Error","Account already closed")
			return
		}
		setContent(res,http.StatusOK,"Account closed, payout",strconv.Itoa(int(payout)))
	}
}

func InitServer() {
	http.HandleFunc("/account", accountAPI) // set router
	err := http.ListenAndServe(":8080", nil) // set listen port
	if err != nil {log.Fatal("ListenAndServe: ", err)}
}
